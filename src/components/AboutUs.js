import React, { Component } from 'react';

import '../css/Pages/AboutUs.css';

import lion from '../img/Lion Artboard.png'

class AboutUs extends Component {
    render() {
        return (
            <div className="AboutUs">
                <section className="info">
                    <div className="container-fluid">
                        <div className="row">
                        <div className="lion">
                            <img className="lion__image" src={lion}></img>
                        </div>
                            <div className="col-lg-12 aboutus">
                                <h1>О НАС</h1>
                            </div>
                            <div className="col-lg-6 offset-lg-3 aboutus__desc">
                                <p>Мы - команда А, А - значит аутисты. 86 лет мы паяли паяльником, теперь нервы стали не к чёрту и мы решили, что была не была,
                                 пора и стол покрасить. Далее мы поссорились и Миша Михаил Мишок стал педиатором. Кто бы мог подумать? Так вышло, но, в
                                 принципе, все остались довольны, ведь педиатор в семье - счастье для ребёнка. Как говорится: "Сегодня - наркоман, завтра -
                                 рэпер". Вообще, типографика сайта мне не нравится, но это потому что так надо, а не потому что я не могу (!!!). Написал
                                 достаточно, хоть премию за фристайл давай. Та шо там говорить - БАН.</p>
                            </div>
                            <div className="col-lg-12 sponsors">
                                <h2>Наши спонсоры</h2>
                            </div>
                            <div className="col-lg-12 sponsors__list">
                                <p>Миша Фиглипуп</p>
                                <p>Ипнокет</p>
                                <p>Григорий, заводи, не болтай лишнего</p>
                                <p>Завожу, мой мистер, Завожу</p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export default AboutUs