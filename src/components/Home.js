import React, { Component } from 'react';
import { BrowserRouter as Router, Route, NavLink, Link, Switch } from "react-router-dom";

import Quest from './quest.js'

import '../css/Pages/Home.css'

import lion from '../img/Lion Artboard.png'

class Home extends Component {
    render() {
        const quest = <Quest className="vishide" />
        return (
            <div className="Home">
                <div className="container-fluid">

                    <div className="row">
                        <div className="col-lg-7 offset-1 more">
                            <div>
                                <h1>ПОПРОБУЙ УВИДЕТЬ <span className="yellow">БОЛЬШЕ</span></h1>
                                <p>Квесты, мероприятия и другие интересные вещи для вас</p>
                                <Link to="/Quests"><button type="submit" className="more__button">К КВЕСТАМ</button></Link>
                            </div>
                        </div>
                        <div className="lion">
                            <img className="lion__image" src={lion}></img>
                        </div>
                    </div>

                    <div className="row ">
                        <div className="col-lg-12 d-flex justify-content-center">
                            <div className="namesort">
                                <p className="verical-text">САМЫЕ ПОПУЛЯРНЫЕ КВЕСТЫ НЕДЕЛИ</p>
                            </div>
                            {quest}
                            {quest}
                            {quest}
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-lg-12 d-flex justify-content-center">
                            <div className="pop-location namesort">
                                <p className="verical-text">САМЫЕ ПОПУЛЯРНЫЕ ЛОКАЦИИ НЕДЕЛИ</p>
                            </div>
                            {quest}
                            {quest}
                            {quest}
                        </div>
                    </div>

                    <div className="row margin">
                        <div className="col-lg-12 d-flex justify-content-center">
                            <div className="pop-mostpop namesort">
                                <p className="verical-text">САМЫЕ ПОПУЛЯРНЫЕ ЗА ВСЁ ВРЕМЯ</p>
                            </div>
                            {quest}
                            {quest}
                            {quest}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Home


































