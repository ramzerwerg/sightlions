import React, { Component } from 'react';

import '../css/Pages/Pay.css'

import lion from '../img/Lion Artboard.png'

class Pay extends Component {
    render() {
        return (
            <div className="PayAchivement">
                <div className="container-fluid">
                    <div className='row d-flex'>

                        <div className="lion">
                            <img className="lion__image" src={lion}></img>
                        </div>

                        <div className="col-lg-12 d-flex justify-content-center">
                            <h1>ОПЛАТА</h1>
                        </div>

                        <div className="col-lg-4 offset-lg-2">
                            <form id="pay" action="#" className="form pay">
                                <input type="card" placeholder="Номер карты" className="form__input" required></input>
                                <input type="name" placeholder="Имя держателя" className="form__input" required></input>
                                <input type="end" placeholder="Срок действия" className="form__input" required></input>
                                <input type="CVV" placeholder="CVV-код" className="form__input" required></input>
                            </form>
                        </div>
                        <div className="col-lg-3 offset-1">
                            <div className="card">
                                <h1>BANKNAME</h1>
                                <div className="card__item d-flex">
                                    <div />
                                    <p>VALID THRU <span>09 / 20</span></p>
                                </div>
                                <h2>0000      0000      0000      0000</h2>
                                <div className="card__item2 d-flex">
                                    <h2>KRISTINA GRIGORIEVA</h2>
                                    <div className="visa d-fle">
                                        <p>VISA</p>
                                    </div>
                                </div>

                            </div>
                            <div className="confirm">
                            <p>Нажимая ГОТОВ, я подтверждаю, что согласен оплачивать gокупки на данном сайте.</p>
                            </div>
                            <div className="d-flex justify-content-end">
                            <button type="submit" className="form__btn" form="pay">ГОТОВО</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Pay