import React, { Component } from 'react';

import '../css/Presets/quest.css'

import star from '../img/star.png'

class Quest extends Component {

    state = {
        hide: "",
        wvalue: "",
        isOpen: true
    }

    render() {
        const style = { width: this.state.wvalue };
        return (
            <div className="quest" style={style}>
                <div className="quest__image">
                    <img ></img>
                </div>
                <div>
                    <h1>ВСЕМ КВЕСТАМ КВЕСТ</h1>
                </div>
                <div className="quest__stars">
                    <img src={star} />
                    <img src={star} />
                    <img src={star} />
                    <img src={star} />
                    <img src={star} />
                </div>
                <div>
                    <p>Квест в интересном районе, где-то около супер магазина. Нужно ходить и ходить, затем просто найти то самое, ну это, и отнести куда скажут.</p>
                </div>
                <button className="quest__button" onClick={this.handleClick}>{this.state.isOpen ? 'ПОДРОБНЕЕ' : 'СВЕРНУТЬ'}</button>
            </div>
        );
    }
    handleClick = () => {
        this.setState({
            isOpen: !this.state.isOpen
        })
        this.state.isOpen == true ? this.setState({ wvalue: "1520px" }) : this.setState({ wvalue: "511px" })
    }
}


export default Quest