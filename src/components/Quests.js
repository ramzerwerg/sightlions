import React, { Component } from 'react';

import Quest from './quest.js'

import '../css/Pages/Quests.css'

import lion from '../img/Lion Artboard.png'

class Quests extends Component {
    render() {
        const quest = <Quest className="vishide" />
        return (
            <div className="Quests">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-lg-5">
                            <div>
                                <ul className="sort__name d-flex">
                                    <li><a href="#">Исторические</a></li>
                                    <li><a href="#">Популярные</a></li>
                                    <li><a href="#">В квеструмах</a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="lion">
                            <img className="lion__image" src={lion}></img>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12 d-flex justify-content-center">
                            <div className="pop namesort">
                                <p className="verical-text">САМЫЕ ПОПУЛЯРНЫЕ КВЕСТЫ</p>
                            </div>
                            {quest}
                            {quest}
                            {quest}
                        </div>
                        <div className="col-lg-12 d-flex justify-content-center">
                            <div className="namesort" />
                            {quest}
                            {quest}
                            {quest}
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-lg-12 d-flex justify-content-center">
                            <div className="history namesort">
                                <p className="verical-text">ИСТОРИЧЕСКИЕ КВЕСТЫ</p>
                            </div>
                            {quest}
                            {quest}
                            {quest}
                        </div>
                        <div className="col-lg-12 d-flex justify-content-center">
                            <div className="namesort" />
                            {quest}
                            {quest}
                            {quest}
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-lg-12 d-flex justify-content-center">
                            <div className="room namesort">
                                <p className="verical-text">В КВЕСТРУМАХ</p>
                            </div>
                            {quest}
                            {quest}
                            {quest}
                        </div>
                        <div className="col-lg-12 d-flex justify-content-center">
                            <div className="namesort" />
                            {quest}
                            {quest}
                            {quest}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Quests