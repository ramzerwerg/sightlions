import React, { Component } from 'react';

import '../css/Pages/Rating.css';

import lion from '../img/Lion Artboard.png'

class Rating extends Component {
    render() {
        return (
            <div className="Rating">
                <div className="container-fluid">
                    <div className="row">
                    <div className="lion">
                            <img className="lion__image" src={lion}></img>
                        </div>
                        <div className="col-lg-12 rating">
                            <h1>РЕЙТИНГИ</h1>
                        </div>
                        <div className="col-lg-12 d-flex justify-content-center">
                            <table className="col-lg-8 rating__item">
                                <tr>
                                    <td>
                                        <p>Имя</p>
                                    </td>
                                    <td>
                                        <p>Всего квестов</p>
                                    </td>
                                    <td>
                                        <p>Рейтинг</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td><img className="profile__img" /></td>
                                    <td><p className="profile__name">Кристина Григорьева</p></td>
                                    <td><p className="profile__quests">27</p></td>
                                    <td><p className="profile__rating">2785</p></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Rating