import React, { Component } from 'react';

import '../css/Pages/Profile.css'

import lion from '../img/Lion Artboard.png'

class Profile extends Component {
    render() {
        return (
            <div className="Profile">
                <div className="container-fluid">
                    <div className="row">
                        
                        <div className="lion">
                            <img className="lion__image" src={lion}></img>
                        </div>

                    </div>
                    <p>Profile</p>
                </div>
            </div>
        )
    }
}

export default Profile