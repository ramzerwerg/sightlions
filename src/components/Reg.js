import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import '../css/Pages/Reg-in.css';
import '../css/Presets/Bar.css';

import lion from '../img/Lion Artboard.png'

class Reg extends Component {

  render() {
    return (
      <div className="Reg"> 
        <section className="registration">
          <div className="container-fluid">
            <div className="row">
              <div className="col-lg-4 offset-lg-4 forms">
                <div className="col-lg-12">
                  <p className="registrations">Регистрация</p>
                </div>
                <form id="regf" action="#" className="form ">
                  <input type="name" placeholder="Имя" className="form__input" required></input>
                  <input type="first_name" placeholder="Фамилия" className="form__input" required></input>
                  <input type="email" placeholder="E-mail" className="form__input" required></input>
                  <input type="password" placeholder="Пароль" className="form__input" required></input>
                </form>
                <div className="col-lg-12 d-flex justify-content-center">
                  <button type="submit" className="form__btn" form="regf">ГОТОВО</button>
                </div>
              </div>
              <div className="lion">
                <img className="lion__image" src={lion}></img>
              </div>
            </div>
          </div>
        </section>
      </div >
    );
  }


}

export default Reg;
