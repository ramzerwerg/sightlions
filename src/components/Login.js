import React, { Component } from 'react';

import '../css/Pages/Reg-in.css';
import '../css/Presets/Bar.css';

import lion from '../img/Lion Artboard.png'

class Login extends Component {

    render() {
        return (
            <div className="Login">
                <section className="registration">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-4 offset-lg-4 forms">
                                <div className="col-lg-12">
                                    <p className="registrations">Вход</p>
                                </div>
                                <form id="regf" action="#" className="form ">
                                    <input type="email" placeholder="E-mail" className="form__input" required></input>
                                    <input type="password" placeholder="Пароль" className="form__input" required></input>
                                </form>
                                <div className="col-lg-12 d-flex justify-content-center">
                                    <button type="submit" className="form__btn" form="regf">ГОТОВО</button>
                                </div>
                            </div>
                            <div className="">
                                <img className="lion" src={lion} />
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        )
    }
}

export default Login