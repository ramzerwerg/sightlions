import React, { Component } from 'react';
import { BrowserRouter as Router, Route, NavLink, Link, Switch } from "react-router-dom";

import '../css/Presets/Header.css';
import '../css/Presets/Bar.css';

import menuimg from '../img/Menu.png'
import search from '../img/search.png'
import lion from '../img/Lion Artboard.png'
import imgacc from '../img/NotReg.png'
import exit from '../img/Exit.png'

import Reg from './Reg.js'
import Home from './Home.js'
import Login from './Login.js'
import Quests from './Quests.js'
import location from './Location.js'
import map from './Map.js'
import Rating from './Rating.js'
import Contacts from './Contacts.js'
import AboutUs from './AboutUs.js'
import Profile from './Profile.js'
import Achivement from './Achivement.js'
import Pay from './Pay.js'


class App extends Component {

  render() {
    return (
      <div className="App">
        <Router>
          <div>

            <input type="checkbox" id="checkbox1" class="gf" hidden></input>

            <section className="bar">
              <nav>
                <ul className="barlist">
                  <li className="bar__exit">
                    <div>
                      <label className='close' for="checkbox1"><img className="exit" src={exit}></img></label>
                    </div>
                  </li >
                  <li className="bar__logo">
                    <p className="logoname">SIGHT<br /><span className='logocolor'>LIONS</span></p>
                  </li>
                  <li className="bar__acc">
                    <img className="acc" src={imgacc}></img>
                    <p className="notreg">Не авторизован</p>
                  </li>
                  <li className="bar__list">
                    <Link to='/Profile'>Профиль</Link>
                  </li>
                  <li className="bar__list">
                    <Link to='/Achivement'>Достижения</Link>
                    </li>
                  <li className="bar__list">
                    <Link to='/Rating'>Рейтинги</Link>
                  </li>
                  <li className="bar__list">
                    <Link to='/Pay'>Оплата</Link>
                  </li>
                  <li className="bar__reg">
                    <Link to="/Reg" className="reg">Регистрация</Link>
                  </li>
                  <li className="bar__reg">
                    <Link to="/Login" className="login">Вход</Link>
                  </li>
                </ul>
              </nav>
            </section>

            <header className="header">
              <div className="container-fluid">
                <div className="row d-flex justify-content-center">

                  <div className="col-lg-1 d-flex justify-content-start">
                    <label for="checkbox1"><img className="menuimg" src={menuimg} /></label>
                  </div>

                  <div className="col-lg-3">
                    <nav>
                      <ul className="menu d-flex justify-content-around">
                        <li className="menu__item">
                          <NavLink to="/Quests/" activeClassName="active">Квесты</NavLink>
                        </li>
                        <li className="menu__item">
                          <NavLink to="/Location/" activeClassName="active">Локации</NavLink>
                        </li>
                        <li className="menu__item last">
                          <NavLink to="/Map/" activeClassName="active">Карта</NavLink>
                        </li>
                      </ul>
                    </nav>
                  </div>

                  <div className="col-lg-2 d-flex justify-content-center">
                    <Link className="logoname" to="/">SIGHT<span className="logocolor">LIONS</span></Link>
                  </div>

                  <div className="col-lg-3">
                    <nav>
                      <ul className="menu d-flex justify-content-around">
                        <li className="menu__item first">
                          <NavLink to="/Rating/" activeClassName="active">Рейтинг</NavLink>
                        </li>
                        <li className="menu__item">
                          <NavLink to="/Contacts/" activeClassName="active">Контакты</NavLink>
                        </li>
                        <li className="menu__item">
                          <NavLink to="/AboutUs" activeClassName="active">О нас</NavLink>
                        </li>
                      </ul>
                    </nav>
                  </div>
                  <div className="col-lg-1 d-flex justify-content-end">
                  <div className="find"></div>
                    <img className="search" src={search} />
                  </div>
                </div>

              </div>
            </header>

            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/Reg" component={Reg} />
              <Route path="/Login" component={Login} />
              <Route path="/Quests" component={Quests} />
              <Route path="/Location" component={location} />
              <Route path="/Map/" component={map} />
              <Route path="/Rating" component={Rating} />
              <Route path="/Contacts" component={Contacts} />
              <Route path="/AboutUs" component={AboutUs} />

              <Route path="/Profile" component={Profile} />
              <Route path="/Achivement" component={Achivement } />
              <Route path="/Pay" component={Pay} />
            </Switch>

            <footer className="footer">
              <div className="container">

                <div className="row justify-content-center">
                  <div className="col-lg-6 d-flex justify-content-center footer__soc">
                    <p className="social">Наши социальные сети</p>
                    <a href="#" className="footer__item">Instagram</a>
                    <a href="#" className="footer__item">VKontakte</a>
                    <a href="#" className="footer__item">Twitter</a>
                  </div>
                  <div className="col-lg-4 d-flex justify-content-center footer__tel">
                    <p className="social">По всем вопросам</p>
                    <p className="social__tel">+7(999) 887 - 12 - 35</p>
                  </div>
                </div>

              </div>
            </footer>
         
          </div>
        </Router>
      </div >
    );
  }


}

export default App;
