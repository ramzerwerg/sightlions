import React, { Component } from 'react';

import '../css/Pages/Contacts.css';

import lion from '../img/Lion Artboard.png'

class Contacts extends Component {
    render() {
        return (
            <div className="Contacts">
                <div className="container-fluid">
                    <div className="row">
                    <div className="lion">
                            <img className="lion__image" src={lion}></img>
                        </div>
                        <div className="col-lg-12 contacts">
                            <h1>КОНТАКТЫ</h1>
                        </div>
                        <div className="col-lg-12 contacts__list">
                        <ul>
                            <li><p>+7 (999) 867 - 12 -35 </p></li>
                            <li><a href="#">Instagram</a></li>
                            <li><a href="#">VKontakte</a></li>
                            <li><a href="#">Twitter</a></li>
                        </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Contacts