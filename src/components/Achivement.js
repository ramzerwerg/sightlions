import React, { Component } from 'react';

import '../css/Pages/Achivement.css'

import lion from '../img/Lion Artboard.png'

class Achivement extends Component {
    render() {
        return (
            <div className="Achivement">
                <div className="container-fluid">
                    <div className="row">

                        <div className="lion">
                            <img className="lion__image" src={lion}></img>
                        </div>

                    </div>
                    <p>Achivement</p>
                </div>
            </div>
        )
    }
}

export default Achivement