import './bootstrap.min.css';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App';
import * as serviceWorker from './serviceWorker';

import { Router, Route } from 'react-router-dom'


ReactDOM.render(
    <App />,
    document.getElementById('root')
);
serviceWorker.unregister();
